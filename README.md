This is a replacement for the FileChooserButton widget which was removed in gtk4.
The widget is a subclass of a normal gtk::Button but with an embedded 
FileChooserDialog which pops up when the button is clicked, storing the
associated file as a property of the button and updating the text of the button
with the basename of the associated file.

The majority of the methods in gtk4::prelude::FileChooserExt are accessible as
wrapper functions. For those few which are not, a reference to the embedded
FileChooserDialog can be obtained using the `dialog()` method.
## Usage
Add to your `Cargo.toml`:
```Yaml
gtk4-file-chooser-button = "0.1"
```
After creation, make sure to set the dialog as transient for the window which the
button resides in. Skipping this step results in odd behavior.
```Rust
# use gtk4-file-chooser-button::FileChooserButton;
# use gtk4::prelude::*;
# fn main() {
let window = gtk4::Window::new();
let vbox = gtk4::Box::new(gtk4::Orientation::Vertical, 5);
window.set_child(Some(&vbox));
let button = FileChooserButton::new(gtk4::FileChooserAction::Open);
vbox.append(&button);
button.dialog().set_transient_for(&window);
window.show();
# }
```
