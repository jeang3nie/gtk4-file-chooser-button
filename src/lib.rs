#![doc = include_str!("../README.md")]
mod imp;

use gtk::{
    gio::{ListModel, File},
    glib::{self, error::Error as GError, clone, GString, Object},
    prelude::*,
    subclass::prelude::*, FileFilter, FileChooserAction,
};

glib::wrapper! {
    /// The FileChooserButton is a widget that lets the user select a file. It
    /// implements the FileChooser interface. Visually, it is a file name with
    /// a button to bring up a GtkFileChooserDialog. The user can then use that
    /// dialog to change the file associated with that button.
    pub struct FileChooserButton(ObjectSubclass<imp::FileChooserButton>)
        @extends gtk::Button, gtk::Widget,
        @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl Default for FileChooserButton {
    fn default() -> Self {
        Self::new(FileChooserAction::Open)
    }
}

impl FileChooserButton {
    /// Creates a new FileChooserButton with the specified `FileChooserAction`
    pub fn new(action: FileChooserAction) -> Self {
        let obj: Self = Object::new(&[("label", &"[None]")])
            .expect("Cannot create file chooser button");
        let dialog = &obj.imp().dialog;
        dialog.set_action(action);
        obj.connect_clicked(clone!(@weak dialog => move |_| {
            dialog.show();
        }));
        dialog.connect_response(clone!(@weak obj as btn => move |dlg,res| {
            if res == gtk::ResponseType::Accept {
                if let Some(file) = dlg.file() {
                    if let Some(name) = file.basename() {
                        btn.set_label(&name.to_string_lossy());
                    }
                    *btn.imp().file.borrow_mut() = Some(file);
                }
            }
            dlg.hide();
        }));
        obj
    }

    /// Adds filter to the list of filters that the user can select between.
    pub fn add_filter(&self, filter: &gtk::FileFilter) {
        self.imp().dialog.add_filter(filter);
    }

    /// Adds a folder to be displayed with the shortcut folders in a file chooser.
    pub fn add_shortcut_folder(&self, folder: &impl IsA<File>) -> Result<(), GError> {
        self.imp().dialog.add_shortcut_folder(folder)
    }

    /// Gets the type of operation that the file chooser is performing.
    pub fn action(&self) -> gtk::FileChooserAction {
        self.imp().dialog.action()
    }

    /// Gets the currently selected option in the ‘choice’ with the given ID.
    pub fn choice(&self, id: &str) -> Option<GString> {
        self.imp().dialog.choice(id)
    }

    /// Gets whether file chooser will offer to create new folders.
    pub fn creates_folders(&self) -> bool {
        self.imp().dialog.creates_folders()
    }

    /// Gets the current folder of chooser as `glib::File`.
    pub fn current_folder(&self) -> Option<File> {
        self.imp().dialog.current_folder()
    }

    /// Gets the current name in the file selector, as entered by the user.
    pub fn current_name(&self) -> Option<GString> {
        self.imp().dialog.current_name()
    }

    /// Gets the `glib::File` for the currently selected file in the file selector.
    pub fn file(&self) -> Option<File> {
        self.imp().file.borrow().clone()
    }

    /// Lists all the selected files and subfolders in the current folder of chooser as `glib::File`.
    pub fn files(&self) -> ListModel {
        self.imp().dialog.files()
    }

    /// Gets the current filter.
    pub fn filter(&self) -> Option<FileFilter> {
        self.imp().dialog.filter()
    }

    /// Gets the current set of user-selectable filters, as a list model.
    pub fn filters(&self) -> ListModel {
        self.imp().dialog.filters()
    }

    /// Gets whether multiple files can be selected in the file chooser.
    pub fn selects_multiple(&self) -> bool {
        self.imp().dialog.selects_multiple()
    }

    /// Queries the list of shortcut folders in the file chooser.
    pub fn shortcut_folders(&self) -> ListModel {
        self.imp().dialog.shortcut_folders()
    }

    /// Removes a ‘choice’ that has been added with gtk_file_chooser_add_choice().
    pub fn remove_choice(&self, id: &str) {
        self.imp().dialog.remove_choice(id);
    }

    /// Removes filter from the list of filters that the user can select between.
    pub fn remove_filter(&self, filter: &FileFilter) {
        self.imp().dialog.remove_filter(filter);
    }

    /// Removes a folder from the shortcut folders in a file chooser.
    pub fn remove_shortcut_folder(&self, folder: &impl IsA<File>) -> Result<(), GError> {
        self.imp().dialog.remove_shortcut_folder(folder)
    }

    /// Sets the type of operation that the chooser is performing.
    pub fn set_action(&self, action: FileChooserAction) {
        self.imp().dialog.set_action(action)
    }

    /// Selects an option in a ‘choice’ that has been added with gtk_file_chooser_add_choice().
    pub fn set_choice(&self, id: &str, option: &str) {
        self.imp().dialog.set_choice(id, option);
    }

    /// Sets whether file chooser will offer to create new folders.
    pub fn set_create_folders(&self, create_folders: bool) {
        self.imp().dialog.set_create_folders(create_folders);
    }

    /// Sets the current folder for chooser from a `glib::File`.
    pub fn set_current_folder(&self, file: Option<&impl IsA<File>>) -> Result<(), GError> {
        self.imp().dialog.set_current_folder(file)
    }

    /// Sets the current name in the file selector, as if entered by the user.
    pub fn set_current_name(&self, name: &str) {
        self.imp().dialog.set_current_name(name);
    }

    /// Sets file as the current filename for the file chooser.
    pub fn set_file(&self, file: &File) -> Result<(), GError> {
        *self.imp().file.borrow_mut() = Some(file.clone());
        self.imp().dialog.set_file(file)
    }

    /// Sets the current filter.
    pub fn set_filter(&self, filter: &FileFilter) {
        self.imp().dialog.set_filter(filter);
    }

    /// Sets whether multiple files can be selected in the file chooser.
    pub fn set_select_multiple(&self, select_multiple: bool) {
        self.imp().dialog.set_select_multiple(select_multiple);
    }

    /// Returns the embedded FileChooserDialog
    pub fn dialog(&self) -> gtk::FileChooserDialog {
        self.imp().dialog.clone()
    }
}
