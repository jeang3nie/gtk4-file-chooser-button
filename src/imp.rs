use {
    gtk::{
        gio::File,
        glib,
        prelude::*,
        subclass::prelude::*,
    },
    std::cell::RefCell,
};

#[derive(Default)]
pub struct FileChooserButton {
    pub dialog: gtk::FileChooserDialog,
    pub file: RefCell<Option<File>>,
}

#[glib::object_subclass]
impl ObjectSubclass for FileChooserButton {
    const NAME: &'static str = "FileChooserButton";
    type Type = super::FileChooserButton;
    type ParentType = gtk::Button;
}

impl ObjectImpl for FileChooserButton {
    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);
        self.dialog.add_action_widget(
            &gtk::Button::with_label("Cancel"),
            gtk::ResponseType::Cancel,
        );
        self.dialog.add_action_widget(
            &gtk::Button::with_label("Accept"),
            gtk::ResponseType::Accept,
        );
    }
}

impl WidgetImpl for FileChooserButton {}
impl ButtonImpl for FileChooserButton {}
